import { AuthenticationService } from './../services/authentication.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  public profile: any;
  profileForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private alertController: AlertController,
    private router: Router,
    private el: ElementRef,
    private loadingController: LoadingController
  ) {
    this.loadProfile();
  }

  ngOnInit() {
    
  }
  
  loadProfile() {
    this.authService.getProfile()
      .subscribe(response => {
        this.profile =response["response"][0];
        (document.getElementById('fname') as HTMLInputElement).value = response["response"][0]["firstname"];
        (document.getElementById('lname') as HTMLInputElement).value = response["response"][0]["lastname"];
        (document.getElementById('phone') as HTMLInputElement).value = response["response"][0]["phone"];
        (document.getElementById('email') as HTMLInputElement).value = response["response"][0]["email"];
        let today = new Date(response["response"][0]["dob"]);
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();
        let todayDate = mm + '-' + dd + '-' + yyyy;
        (document.getElementById('dob') as HTMLInputElement).value = response["response"][0]["dob"];
      }, err => {
        console.log(err);
      }, () => {
        console.log('error');
      }); 
  }

  updateProfile(){
    const firstname = this.el.nativeElement.querySelector('#firstname').value;
    const lastname = this.el.nativeElement.querySelector('#lastname').value;
    const phone = this.el.nativeElement.querySelector('#phone').value;
    const email = this.el.nativeElement.querySelector('#email').value;
    const dob = this.el.nativeElement.querySelector('#dob').value;

    this.authService.updateProfile(firstname,lastname,phone,email,dob)
      .subscribe(response => {
        this.alertSuccess("Profile updated..!");
      }, err => {
        console.log(err);
      }, () => {
        console.log('error');
      }); 

  }

  async alertSuccess(msg){
    const alert = await this.alertController.create({
      header: 'UPDATED',
      message: msg,
      buttons: ['OK'],
    });
    await alert.present(); 
  }

  async logout() {
    await this.authService.logout();
    this.router.navigateByUrl('/', { replaceUrl: true });
  }
}
