import {  Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthenticationService,ApiImage } from './../services/authentication.service';

import { AlertController, LoadingController } from '@ionic/angular';
import { Capacitor, Plugins, CameraResultType, CameraSource, FilesystemDirectory  } from '@capacitor/core';
import { Platform, ActionSheetController } from '@ionic/angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
const { Camera, Filesystem } = Plugins;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  
  private prescriptionForm : FormGroup;
  prescription : any;
  images : any;
  imagesPath="";
  public latitude :  number;
  public longitude:  number;

  public pharmacy_names : any;
  public pahrmacy_number : any;
  public firstname : any;
  public lastname : any;
  public dob : any;
  public lat : any;
  public lon : any;

  public filepath : any;
  public fileblob : any;
  public filename : any;
  public filetype : any;
  pharmacylist=[];
  
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService, 
    private alertController: AlertController,
    private loadingController: LoadingController,
    private plt: Platform, 
    private router: Router,
    private el: ElementRef,
    private geolocation: Geolocation,
    private iab: InAppBrowser,
    private actionSheetCtrl: ActionSheetController) {

  }

  ngOnInit() {
    this.loadPharmacy();
    this.loadProfile();
  }
  
  loadProfile() {
    this.authService.getProfile()
      .subscribe(response => {
        //this.firstname = response["response"][0]['firstname'];
        //this.lastname = response["response"][0]['lastname'];
        this.dob = response["response"][0]['dob'];
      }, err => {
        console.log(err);
      }, () => {
        console.log('error');
      });  
  }

  loadPharmacy() {
    this.geolocation.getCurrentPosition({enableHighAccuracy: true, timeout: 5000, maximumAge: 0}).then((resp) => {
      let lat = resp.coords['latitude'];
      let lon =resp.coords['longitude'];

      this.lat = lat;
      this.lon = lon;

      this.authService.getPharmacy(lat,lon).subscribe(response => {
        for(let key in response["results"]){
          let storeName=response["results"][key]['store']["address"]["locationName"];
          let strokeId=response["results"][key]['storeNumber'];
          if(storeName!=null){
            this.pharmacylist.push({"code":strokeId,"name":storeName});
          }          
        }
      }, err => {
        console.log(err);
      }, () => {
        console.log('completed');
      });      
     }).catch((error) => {
       console.log('Error getting location', error);
     });    
  }

  async selectImageSource() {
    const buttons = [
      {
        text: 'Take Photo',
        icon: 'camera',
        handler: () => {
          this.addImage(CameraSource.Camera);
        }
      },
      {
        text: 'Choose From Photos Photo',
        icon: 'image',
        handler: () => {
          this.addImage(CameraSource.Photos);
        }
      }
    ];    
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons
    });
    await actionSheet.present();
  }
 
  async addImage(source: CameraSource) {
    const image = await Camera.getPhoto({
      quality: 60,
      allowEditing: true,
      resultType: CameraResultType.Base64,
      source
    });
    let base64Image = 'data:image/jpeg;base64,' + image.base64String;
    const blobData = this.b64toBlob(image.base64String, `image/${image.format}`);
    this.images=base64Image;
    this.authService.addTmp(base64Image).subscribe(res => {
      this.imagesPath=res['profile_url'];
      // this.images="";
    });    
  }
  
 
  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
 
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
 
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
 
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
 
    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
 
  async alertSuccess(msg){
    const alert = await this.alertController.create({
      header: 'SUCCESS',
      message: msg,
      buttons: ['OK'],
    });
    await alert.present(); 
  }
  async alertError(msg){
    const alert = await this.alertController.create({
      header: 'ERORRS',
      message: msg,
      buttons: ['Dismiss'],
    });
    await alert.present(); 
  }

  async uploadFileServer() {
    let mydob = new Date(this.dob);
    let dob = mydob.getFullYear() + '-' + mydob.getMonth() + '-' + mydob.getDate();
    let pharmacy = this.el.nativeElement.querySelector('#pharmacy_name').value;
    this.pahrmacy_number = this.pharmacylist[Number(pharmacy)]["code"];
    this.pharmacy_names = this.pharmacylist[Number(pharmacy)]["name"];  
    let fname = this.el.nativeElement.querySelector('#firstname').value;
    let lname = this.el.nativeElement.querySelector('#lastname').value;
    let medicationname = this.el.nativeElement.querySelector('#medicationname').value;
    let dose = this.el.nativeElement.querySelector('#dose').value;
    let rx_number = this.el.nativeElement.querySelector('#rx_number').value;
    let prescribedby = this.el.nativeElement.querySelector('#prescribedby').value;
    
    if(this.imagesPath==""){
      alert("Choose Image");
    }else if(fname==""){
      alert("Please Entry First Name");
    }else if(lname==""){
      alert("Please Entry Last Name");
    }else if(medicationname==""){
      alert("Please Entry Medication Name");
    }else if(dose==""){
      alert("Please Entry Dose");
    }else if(rx_number==""){
      alert("Please Entry Rx Number");
    }else if(prescribedby==""){
      alert("Please Entry Prescribed By");
    }else if(pharmacy==undefined){
      alert("Choose Pharmacy");
    }else{
     
      const loading = await this.loadingController.create();
      await loading.present();

      this.authService.uploadImage(fname, lname, dob, this.pahrmacy_number, this.pharmacy_names,medicationname,dose,rx_number,prescribedby, this.imagesPath).subscribe(response => {
          if(response["status"]==200){
            
            let url = 'http://54.177.78.231/walgreens/refillapi_jk.php?rx_number='+rx_number+'&lat='+this.lat+'&lng='+this.lon+'&base64image='+this.imagesPath;
            const  browser = this.iab.create(url);
            browser.on('loadstop').subscribe(event => {
              if(event['status']=="SUCCESS")
              {
                this.router.navigateByUrl('/tabs/prescription', { replaceUrl: true });
              }              
              loading.dismiss();
            });           
            browser.close();
            //   this.el.nativeElement.querySelector('#pharmacy_name').value=0;
            //   this.el.nativeElement.querySelector('#firstname').value="";
            //   this.el.nativeElement.querySelector('#lastname').value="";
            //   this.el.nativeElement.querySelector('#medicationname').value="";
            //   this.el.nativeElement.querySelector('#dose').value="";
            //   this.el.nativeElement.querySelector('#rx_number').value="";
            //   this.el.nativeElement.querySelector('#prescribedby').value="";
            //   this.alertSuccess("Inserted successfully..!");  
            //   loading.dismiss();       

            // this.authService.uploadRefill(rx_number,this.lat,this.lon,this.imagesPath).subscribe(ress => {
            //   let url = 'http://54.177.78.231/walgreens/'+ress[0]['msg'];
            //   this.iab.create(url);
            //   this.el.nativeElement.querySelector('#pharmacy_name').value=0;
            //   this.el.nativeElement.querySelector('#firstname').value="";
            //   this.el.nativeElement.querySelector('#lastname').value="";
            //   this.el.nativeElement.querySelector('#medicationname').value="";
            //   this.el.nativeElement.querySelector('#dose').value="";
            //   this.el.nativeElement.querySelector('#rx_number').value="";
            //   this.el.nativeElement.querySelector('#prescribedby').value="";
            //   this.alertSuccess("Inserted successfully..!");  
            //   loading.dismiss();           
            // });

           
            //this.router.navigateByUrl('/tabs/prescription', { replaceUrl: true });

           
            //this.router.navigateByUrl('/tabs/prescription', { replaceUrl: true });
          }
          else{
            this.alertSuccess("Not inserted");
          } 
      });
      // await loading.dismiss();
    }    
  }

  getSelectedOptionText(x) {
    let tmpid = x.target.value;
    this.pahrmacy_number = this.pharmacylist[tmpid]["code"];
    this.pharmacy_names = this.pharmacylist[tmpid]["name"];         
  }

  async logout() {
    await this.authService.logout();
    this.router.navigateByUrl('/', { replaceUrl: true });
  }

}
