import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  credentials: FormGroup;
 
  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private alertController: AlertController,
    private router: Router,
    private loadingController: LoadingController
  ) {}
 
  ngOnInit() {
    this.credentials = this.fb.group({
      user: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  
  register() {
    this.router.navigateByUrl('/registration', { replaceUrl:true });
  }
  forgetPassword() {
    this.router.navigateByUrl('/forget', { replaceUrl:true });
  }
  
  async login() {
    const loading = await this.loadingController.create();
    await loading.present();
    
    this.authService.login(this.credentials.value).subscribe(
      async (res) => {
        await loading.dismiss();   
        if(res["status"]==400){
          const alert = await this.alertController.create({
            header: 'Username and password not matched',
            message: res.error.error,
            buttons: ['OK'],
          }); 
          await alert.present();
        } else{
          this.router.navigateByUrl('/tabs', { replaceUrl: true });
        }
        
      },
      async (res) => {
        await loading.dismiss();
        const alert = await this.alertController.create({
          header: 'Login failed',
          message: res.error.error,
          buttons: ['OK'],
        }); 
        await alert.present();
      }
    );
  }
  
  // Easy access for form fields
  get user() {
    return this.credentials.get('user');
  }
  
  get password() {
    return this.credentials.get('password');
  }
}