import { AuthenticationService } from '../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ConfirmedValidator } from "./validate-password";
 
@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  credentials: FormGroup;
 
  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private alertController: AlertController,
    private router: Router,
    private loadingController: LoadingController
  ) {}
 
  ngOnInit() {
    this.credentials = this.fb.group({
      fullname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6),Validators.maxLength(30)]],
      confirmpassword: [ '', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ])],
    },{
      validator: ConfirmedValidator('password', 'confirmpassword')
   });
  }
  loginback() {
    this.router.navigateByUrl('/login', { replaceUrl:true });
  }
  async registration() {
    const loading = await this.loadingController.create();
    await loading.present();
    
    this.authService.registration(this.credentials.value).subscribe(
      async (res) => {
        await loading.dismiss();        
        this.router.navigateByUrl('/login', { replaceUrl: true });
        await loading.dismiss();
        const alert = await this.alertController.create({
          header: 'Successfully registered..!!',
          message: res.error.error,
          buttons: ['OK'],
        });
 
        await alert.present();

      },
      async (res) => {
        await loading.dismiss();
        const alert = await this.alertController.create({
          header: 'Registration Failed',
          message: res.error.error,
          buttons: ['OK'],
        });
 
        await alert.present();
      }
    );
  }
 
  // Easy access for form fields
  get fullname() {
    return this.credentials.get('fullname');
  }
  get email() {
    return this.credentials.get('email');
  }
  
  get password() {
    return this.credentials.get('password');
  }
  get confirmpassword() {
    return this.credentials.get('confirmpassword');
  }

}