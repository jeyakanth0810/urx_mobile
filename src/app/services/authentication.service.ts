import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, tap, switchMap } from 'rxjs/operators';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';
 
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

const walgreensurl ="http://54.177.78.231/walgreens";
const url ="http://54.177.78.231:5000/api";
const imgurl ="http://54.177.78.231:7000";
 
const TOKEN_KEY = 'token';

export interface ApiImage {
  _id: string;
  firstname: string;
  lastname: string;
  dob: Date;
  imagefile: string;
  pharmacy_name: string;
  pahrmacy_number: string;
  
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  token = '';
 
  constructor(private http: HttpClient) {
    this.loadToken();
  }
  
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  

  async loadToken() {
    const token = await Storage.get({ key: TOKEN_KEY });    
    if (token && token.value) {
      console.log('set token: ', token.value);
      this.token = token.value;
      this.isAuthenticated.next(true);
    } else {
      this.isAuthenticated.next(false);
    }
  }

  login(credentials: {user, password}): Observable<any> {
    return this.http.post(url+'/login', credentials, this.httpOptions).pipe(
      tap(async (res:  any ) => {
          if (res["status"]==200) {
            this.token = res["response"];
            Storage.set({key: TOKEN_KEY, value: res["response"]});
            this.isAuthenticated.next(true);
          }else {
            this.isAuthenticated.next(false);
          }
      })
    );    
  };
 
  registration(credentials: {fullname, email, password}): Observable<any> {
    return this.http.post(url+'/registration', credentials, this.httpOptions).pipe(
      tap(async (res:  any ) => {
          if (res["status"]==200) {
            return true;
          }
          if (res["status"]==400) {
            return false;
          }
      })
    );    
  };

  forget(credentials: {email}): Observable<any> {
    return this.http.post(url+'/forget', credentials, this.httpOptions).pipe(
      tap(async (res:  any ) => {
          if (res["status"]==200) {
            this.token = res["response"];
            Storage.set({key: TOKEN_KEY, value: res["response"]});
            this.isAuthenticated.next(true);
          }else {
            this.isAuthenticated.next(false);
          }
      })
    );    
  };
  getProfile(): Observable<any> {    
    return this.http.post(url+'/profile', {'usertoken':this.token}, this.httpOptions).pipe(
      tap(async (res:  any ) => {

        if (res["status"]==200 && res!="null") {
          return res;
        }
      })
    );
  };

  profile(): Observable<any> {    
    return this.http.post(url+'/profile', {'usertoken':this.token}, this.httpOptions).pipe(
      tap(async (res:  any ) => {

        if (res["status"]==200 && res!="null") {
          return res;
        }
      })
    );
  };
  updateProfile(firstname,lastname,phone,email,dob) {
    return this.http.post(url+'/update', {'usertoken':this.token,'firstname':firstname,'lastname':lastname,'phone':phone,'email':email,'dob':dob}, this.httpOptions).pipe(
      tap(async (res:  any ) => {
        console.log(res);
        return res;
      })
    );
  }
  getPharmacy(lat,lng): Observable<any> {
    const la = "lat=12.992512&lng=80.20295680000001";
    // const la = "lat="+lat+"&lng="+lng;
    return this.http.get(walgreensurl+'/apis.php?'+la).pipe(
      tap(async (res:  any ) => {
          return res;
      })
    );    
  };
  // Upload images Start

  getImages(): Observable<any> {
    return this.http.post(url+'/listscan', {'usertoken':this.token}, this.httpOptions).pipe(
      tap(async (res:  any ) => {
        if (res["status"]==200 && res!="null") {
          return res;
        }
      })
    );
  };
  addTmp(imageFile){
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
    };   
    return this.http.post(imgurl+'/upload/image',{'usertoken':this.token,'base64image':imageFile},this.httpOptions).pipe(
      tap(async (res:  any ) => {
        // console.log("Image add to new URL");
        // console.log(res);
        return res;
      })
    );
  }
  // addImage(imageFile) {
  //   const httpOptions = {
  //     headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
  //   };
    
  //   return this.http.post(imgurl+'/upload/image', {'usertoken':this.token,'base64image':imageFile}, httpOptions).pipe(
  //     tap(async (res:  any ) => {
  //       console.log("Image add to new URL");
  //       console.log(res);
  //       return res;
  //     })
  //   );
  // }
  // addImage(filepath,blobData, ext, name) {
  //   return this.http.post(url+'/addimage', {'usertoken':this.token,'imagefile':name}, this.httpOptions).pipe(
  //     tap(async (res:  any ) => {
  //       return res;
  //     })
  //   );
  // }

  uploadImage(firstname,lastname,dob,pahrmacy_number, pharmacy_name,medicationname,dose,rx_number,prescribedby, filename) {
    const formData = {
      'usertoken':this.token,
      'firstname':firstname,
      'lastname':lastname,
      'dob':dob,
      'pahrmacy_number':pahrmacy_number,
      'pharmacy_name':pharmacy_name,
      'medicationname':medicationname,
      'dose':dose,
      'rx_number':rx_number,
      'prescribedby':prescribedby,
      'imagefile':filename
    };
    const body=JSON.stringify(formData);
    return this.http.post(url+'/addscan', body, this.httpOptions).pipe(
      tap(async (res:  any ) => {
        return res;
      })
    );

  }
  uploadRefill(lat,lng,filename) {
    return this.http.get(walgreensurl+'/refillapi_test.php?lat='+lat+'&lng='+lng+'&base64image='+filename).pipe(
      tap(async (res:  any ) => {
        return res;
        
      })   
    );
  }
 
  callbackRefill(token,landingUrl,lag,lng,filename){
    const config = {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS"
      }
    };
    const formData = {
      'affId':"cGJRwksFQR1WbFmo4YueUnAl6d1EhD8J",
      'token':token,
      'lat':lag,
      'lng':lng,
      'rxImg':filename,
      // 'fname':"",
      // 'lname':"",
      // 'dob':"",
      // 'phoneNbr':"",
      // 'pharmacyNbr':"",
      // 'trackingId':"",
      'appId':"transferByScan",
      'act':"transferRxHome"
      // 'appVer':"1.0",
      // 'devInf':""
    };
    const bodys=JSON.stringify(formData);
    return this.http.post(landingUrl, bodys, config).pipe(
      tap(async (res:  any ) => {
        console.log(res);
        return res;
      })
    );    
  
  }

  uploadImageFile(file: File) {
    const ext = file.name.split('.').pop();
    const formData = new FormData();
    formData.append('usertoken', this.token);
    formData.append('imagesfile', file);
    formData.append('name', file.name);
 
    return this.http.post(url+`/addimage`, formData, this.httpOptions);
  }
 
  deleteImage(id) {
    return this.http.delete(url+`/image/${id}`);
  }
  // Upload Image End
 
  logout(): Promise<void> {
    this.isAuthenticated.next(false);
    return Storage.remove({key: TOKEN_KEY});
  }
}

