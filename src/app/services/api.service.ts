import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
 
export interface ApiImage {
  _id: string;
  name: string;
  createdAt: Date;
  url: string;
}
 
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url = 'http://54.177.78.231:5000';
 
  constructor(private http: HttpClient) { }
 
  getImages() {
    return this.http.get<ApiImage[]>(`${this.url}/upload`);
  }
 
  uploadImage(blobData, name, ext) {
    const formData = new FormData();
    formData.append('scan', blobData, `myimage.${ext}`);
    formData.append('name', name);
 
    return this.http.post(`${this.url}/upload`, formData);
  }
 
  uploadImageFile(file: File) {
    const ext = file.name.split('.').pop();
    const formData = new FormData();
    formData.append('file', file, `myimage.${ext}`);
    formData.append('name', file.name);
 
    return this.http.post(`${this.url}/upload`, formData);
  }
 
  deleteImage(id) {
    return this.http.delete(`${this.url}/upload/${id}`);
  }
}