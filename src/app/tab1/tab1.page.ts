import { AuthenticationService } from './../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
 
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  credentials: FormGroup;
  // prescription : any;
  public items: Array<{ medicationname: string; image_location: string;dose: string;rx_number: string; prescribedby: string;first_name: string;last_name: string;created_date: string }> = [];
  public prescription: Array<{ medicationname: string; image_location: string;dose: string;rx_number: string; prescribedby: string;first_name: string;last_name: string;created_date: string }> = [];
  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private alertController: AlertController,
    private router: Router,
    private loadingController: LoadingController
  ) {
    
  }
    
  ngOnInit() {    
    this.loadImages();
  }
  async loadImages() {
    const loading = await this.loadingController.create();
    await loading.present();
    this.authService.getImages().subscribe(response => {
      for (let i = 0; i < response["response"].length; i++) {
        this.items.push({
          medicationname: response["response"][i]["medicationname"].toUpperCase(),
          image_location: response["response"][i]["image_location"],
          dose: response["response"][i]["dose"],
          rx_number: response["response"][i]["rx_number"],
          prescribedby: response["response"][i]["prescribedby"],
          first_name: response["response"][i]["first_name"],
          last_name: response["response"][i]["last_name"],
          created_date: response["response"][i]["created_date"]
        });
      }
      this.prescription =this.items;
      console.log(this.items);
    }, err => {
      console.log(err.message);
    }, () => {
      console.log('completed');
    });

    await loading.dismiss();
  }
  onSearchTerm(ev: CustomEvent) {
    this.items = this.prescription;
    const val = ev.detail.value;

    if (val && val.trim() !== '') {
      this.items = this.items.filter(term => {
        return term.medicationname.toUpperCase().indexOf(val.trim().toUpperCase()) > -1;
      });
    }
  }
  doRefresh(event) {
    console.log('Begin async operation');
    setTimeout(() => {
      console.log('Async operation has ended');
      this.loadImages();
      event.target.complete();
    }, 2000);
  }
  
}