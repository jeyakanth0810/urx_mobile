import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyprescriptionPage } from './myprescription.page';

const routes: Routes = [
  {
    path: '',
    component: MyprescriptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyprescriptionPageRoutingModule {}
