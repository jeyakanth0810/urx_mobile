import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyprescriptionPageRoutingModule } from './myprescription-routing.module';

import { MyprescriptionPage } from './myprescription.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyprescriptionPageRoutingModule
  ],
  declarations: [MyprescriptionPage]
})
export class MyprescriptionPageModule {}
