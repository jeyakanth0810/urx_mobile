import { Component, OnInit } from '@angular/core';
import { AuthenticationService,ApiImage } from './../services/authentication.service';

@Component({
  selector: 'app-myprescription',
  templateUrl: './myprescription.page.html',
  styleUrls: ['./myprescription.page.scss'],
})
export class MyprescriptionPage implements OnInit {

  prescription : any;
  constructor(private authService: AuthenticationService,) { }

  ngOnInit() {
    this.loadImages();
  }
  loadImages() {
    this.authService.getImages().subscribe(response => {
      this.prescription =response["response"];
    }, err => {
      console.log(err.message);
    }, () => {
      console.log('completed');
    });
  }

}
